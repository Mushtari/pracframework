package Utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public class BasePage {
	
	public WebDriver driver;
	@BeforeMethod
	public void openBrowser(){
		Properties p = new Properties();
		try {
			FileInputStream fs = new FileInputStream(System.getProperty(("user dir")+"/Application.properties"));
			p.load(fs);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"/Drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get("https://www.google.com/");
	}
	@AfterMethod
	public void closeBrowser(){
		driver.close();
	}
}
