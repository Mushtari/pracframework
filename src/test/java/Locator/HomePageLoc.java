package Locator;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePageLoc {

	@FindBy(name="q")
	private WebElement searchOnGoogle;
	
	@FindBy(xpath="//a")
	private List<WebElement> allLinks;
	
	
	public WebElement returnSearchOnGoogle(){
		return searchOnGoogle;
	}
	public List<WebElement> returnAllLinks(){
		return allLinks;
	}
}
