package Page;



import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import Locator.HomePageLoc;

public class HomePage extends HomePageLoc{

	public WebDriver driver;
	
	public HomePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
		
		
	public void searchOnGoogle(String text){
		
		returnSearchOnGoogle().sendKeys(text);
		returnSearchOnGoogle().sendKeys(Keys.ENTER);
		
	}
	public void getAllLinks(){
		for(WebElement ele :returnAllLinks()){
			if(ele.getText().compareToIgnoreCase("selenium")==0){
				ele.getText();
			}else{
				//
			}
		}
	}
	
}
